import React, { Component } from 'react';
import MenuJson from '../data/menu.json';

class Header extends Component {
	constructor(props) {
		super(props);

		this.state = {
			items: []
		}
		
		this.openMenu = this.openMenu.bind(this);
	}

	openMenu() {
		const appBody = document.body;
		
		appBody.classList.add('menu-is-visible');

		document.querySelector('.app-header-backdrop').addEventListener('click', function() {
			appBody.classList.remove('menu-is-visible');
		});
	}

	componentDidMount() {
		this.setState({
			items: MenuJson
		})
	}

	render() {
		return (
			<header className="app-header">
				<button className="app-menu-trigger" onClick={this.openMenu}></button>

				<nav className="app-menu">
					<ul>
						{this.state.items.map((item, i) => (
							<li key={i}>
								<a href={item.url}>{item.desc}</a>
								{item.sub.length !== 0 ?
									<ul>
										{item.sub.map((item, i) => (
											<li key={i}>
												<a href={item.url}>{item.desc}</a>

												{item.sub.length !== 0 ?
													<ul>
														{item.sub.map((item, i) => (
															<li key={i}><a href={item.url}>{item.desc}</a></li>
														))}

														{item.sub.length !== 0 ? 
															<ul>
																{item.sub.map((item, i) => (
																	<li key={i}><a href="item.url">{item.desc}</a></li>
																))}
															</ul>
															: console.log('Não há mais itens')
														}
													</ul>
													: console.log('Não há mais itens')
												}
											</li>
										))}
									</ul>

									: console.log('Não há mais itens')
								}
							</li>
						))}
					</ul>
				</nav>

				<div className="app-header-backdrop"></div>
			</header>
		);
	}
}

export default Header;