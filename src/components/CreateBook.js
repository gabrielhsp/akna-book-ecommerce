import React, { Component } from 'react';
import axios from 'axios';

class CreateBook extends Component {
	constructor(props) {
		super(props);

		this.state = {};

		this.handleSubmit = this.handleSubmit.bind(this);
	}

	onlyTextChar(e) {
		const re = /^[A-z À-ÿ!@#$%^&)(+=._-]+$/g;

		if (!re.test(e.key)) { e.preventDefault(); }
	}

	onlyNumbersChar(e) {
		const re = /^[0-9.,]/;

		if (!re.test(e.key)) { e.preventDefault(); }
	}

	handleSubmit(event) {
		event.preventDefault();

		const message = document.querySelector('.app-form-message');

		axios({
			method: 'POST',
			url: '//lab01.akna.com.br/testes/livros.php',
			data: {
				titulo: this.refs.title.value,
				autor: this.refs.author.value,
				preco: this.refs.price.value
			}
		})
		.then((response) => {
			if (response.status === 200) {
				message.textContent = 'Cadastro feito com sucesso';
				message.classList.remove('-error');
				message.classList.add('-success');
				
				this.refs.createBookForm.reset();
				
				setTimeout(function() {
					message.textContent = '';
					message.classList.remove('-success');
				}, 2500);
			} else {
				message.textContent = 'Não foi possível fazer o cadastro';
				message.classList.remove('-success');
				message.classList.add('-error');
			}
		})
		.catch((error) => {
			console.log('error', error);
		});
	}

	render() {
		return (
			<section className="app-register app-column">
				<div className="app-register-wrapper">
					<h1 className="app-title">Cadastro de livros</h1>
					<p className="app-text -small">(*) Campos obrigatórios</p>

					<form className="app-form" ref="createBookForm" onSubmit={this.handleSubmit}>
						<input className="app-input" ref="title" placeholder="Título do livro*" type="text" required onKeyPress={(e) => this.onlyTextChar(e)} />
						<input className="app-input" ref="author" placeholder="Autor do livro*" type="text" required onKeyPress={(e) => this.onlyTextChar(e)} />
						<input className="app-input" ref="price" placeholder="Preço do livro*" type="text" required onKeyPress={(e) => this.onlyNumbersChar(e)} />
						<button className="app-form-submit" type="submit">Cadastrar</button>
					</form>

					<span className="app-form-message"></span>
				</div>
			</section>
		);
	}
}

export default CreateBook;