import React, { Component } from 'react';
import axios from 'axios';

class ListBooks extends Component {
	constructor(props) {
		super(props);

		this.state = {
			books: []
		}

		this.decodeChars = this.decodeChars.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}

	decodeChars(str) {
		return decodeURIComponent(escape(str));
	}

	handleClick(e, data) {
		axios.delete(`//lab01.akna.com.br/testes/livros.php?id=${data.id}`)
			.then((response) => {
				axios.get('//lab01.akna.com.br/testes/livros.php')
					.then((response) => {
						this.setState({
							books: response.data
						});
					});
			})
			.catch((error) => {
				console.log('error', error);
			});
	}

	componentDidMount() {
		axios.get('//lab01.akna.com.br/testes/livros.php')
		.then((response) => {
			this.setState({
				books: response.data
			});
		});
	}

	render() {
		return (
			<section className="app-list app-column">
				<div className="app-list-items">
					{this.state.books.map((book, i) => (
						<div className="app-list-item" key={i} data-id={book.id}>
							<h2 className="app-list-title">{this.decodeChars(book.titulo)}</h2>
							<span className="app-list-author">{this.decodeChars(book.autor)}</span>
							<span className="app-list-price">{`R$ ${book.preco}`}</span>
							<span className="app-list-remove" data-key={i} onClick={(e) => this.handleClick(e, book)}>Remover livro</span>
						</div>
					))}
				</div>
			</section>
		);
	}
}

export default ListBooks;