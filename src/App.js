import React, { Component } from 'react';
import Header from './components/Header';
import CreateBook  from './components/CreateBook';
import ListBooks from './components/ListBooks';
import './App.css';

class App extends Component {
	render() {
		return (
			<div className="app">
				<Header />
				<CreateBook />
				<ListBooks />
			</div>
		);
	}
}

export default App;