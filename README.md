# Akna Book E-commerce

Uma simples aplicação utilizando React para criar um CRUD utilizando API REST em PHP. O projeto foi criado utilizando [Create React App](https://github.com/facebookincubator/create-react-app) como base.

## Rodando o projeto

Dentro da raiz do projeto, aba seu terminal favorito e rode o comando: <br />

`npm install`

Este projeto utiliza `npm scripts` para rodar as tasks de desenvolvimento e produção.

Para rodar o projeto em ambiente de desenvolvimento, execute o comando: <br />

`npm start`

Este comando iniciará um servidor dentro da porta 3000 automaticamente com livereload para as alterações feitas dentro da estrutura de arquivos.